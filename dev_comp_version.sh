#!/bin/bash
echo   
echo ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
echo comparing robot $1 to version $2  
echo ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
echo
echo
echo ----------------------------------------------------------

echo
echo ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
echo Grabbing package lists
echo ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
echo
ssh -l administrator $1 "apt --installed list | tail -n+2" > ./software_versions/$1.lst
./compare_ubuntu_apt.pl ./software_versions/$1.lst ./software_versions/$2.lst

