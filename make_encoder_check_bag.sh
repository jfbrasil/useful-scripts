#!/bin/bash
# This script will create a bag file for sanity checking the encoder data on an OTTO 1500.
# user input 1 is a string to append to the beginning of the bag file name. Suggested that you use
# robot hostname and velocity of robot during test. For example at a terminal type,
# $./make_encoder_check_bag.sh robot-ph-cpe22-06_1.0mps

rosbag record -j /right/encoder /left/encoder /right/status/speed /left/status/speed /right/velocity /left/velocity -o $1_encoder_check
