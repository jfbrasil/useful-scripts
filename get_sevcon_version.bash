#!/bin/bash

function get_version {
  NODE_ID=$1
  INDEX=$2
  TYPE=$3

  HEX="0x$(rs-canopen-sdo-upload can0 $NODE_ID $INDEX 0)"
  echo -e "\t${TYPE}:\t${HEX}"
}

function get_versions {
  NODE_ID=$1

  get_version $NODE_ID 5501 "Internal ROM Checksum"
  # seems to be unused - abencz
  #get_version $NODE_ID 5502 "External ROM Checksum"
  get_version $NODE_ID 5620 "Configuration Checksum"
}

echo "Left Sevcon:"
get_versions 02

echo "Right Sevcon"
get_versions 03
