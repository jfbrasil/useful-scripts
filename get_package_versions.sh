# Script to print the current package versions of autonomy core packages on the system
# Only checks binary versions

#!/bin/bash

ROS_PACKAGES=(
move_base                 # navigation
occupancy_grid_utils 
ompl_informed
robot_localization
flirtlib_ros
cpr_acado
cpr_sbpl

clearpath_gazebo_worlds
control_selection
autonomy_msgs             # cpr_msgs
geometry_layer            # cpr_navigation_plugins
cpr_pathtracker
laser_target_tracker      # cpr_perception
localization              # cpr_slam
cpr_spatial
cpr_std_srvs
error_handling
map_data_tools
map_interface
multimaster_tools
ros_licensing
strategy_management

blackwidow_navigation
jackal_navigation_cpr
rocksteady_navigation
geam_navigation

mongodb_store
rviz
wireless_watcher          # wireless

ros_controllers
s3000_laser

audio_indication
canfestival_ros
cpr_computer_monitor
cpr_visualization
ethernetip_ros
led_array
rocksteady
rocksteady_desktop
rocksteady_robot
rocksteady_simulator
rocksteady_teleop
velocity_interface        # rocksteady_velocity_interface
rqt_clearpath_plugins
visual_indication
rocksteady_appliance
)

for pkg in "${ROS_PACKAGES[@]}"
do
  apt_name=ros-indigo-$(echo $pkg | sed "s/_/-/g")
  pkg_info=$(dpkg -s $apt_name 2> /dev/null)
  if [ "$?" = "0" ]; then
    version=$(echo -n "$pkg_info" | grep Version | cut -d " " -f 2)
    echo "$apt_name - $version"
  else
    echo "$apt_name - Not Found"
  fi
done
