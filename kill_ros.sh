#!/bin/bash

# This script will kill all processes with the letters ros in them 
# except those that also have the word chrome in the name.  
kill -9 $(ps aux | grep 'ros' | grep -v 'chrome' | awk '{print $2}')
