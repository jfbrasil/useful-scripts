#!/bin/sh
# This script does the following:
# Grabs the parameters and cached map
# Starts a bag file and waits for the user to complete it
# Compresses everything
# Cleans up after itself, leaving a single file to send

# Work out a name for the files
LOG_NAME=support-`date +'%F-%k-%M-%S'`

# Dump the parameters and map (and throw an error if ROS isn't running)
rosparam dump params.txt
if [ $? -ne 0 ]; then
  echo "ROS is not running! Inform Clearpath Support of this issue."
  exit;
fi;


cp /var/tmp/ros/cached_map.map cached_map.map

# Create initial combined file
tar -cf ~/$LOG_NAME.tar params.txt cached_map.map
rm params.txt
rm cached_map.map

# Fork rosbag
# Code: http://veithen.github.io/2014/11/16/sigterm-propagation.html
trap 'kill -TERM $PID' TERM INT
echo "Press ENTER to start recording and CTRL-C to finish recording"
sed -n q </dev/tty # Catches the ENTER
rosbag record -a -O $LOG_NAME &
PID=$!
wait $PID
trap - TERM INT
wait $PID
EXIT_STATUS=$?

# Wait for bag file to be closed
echo "Closing log file..."
while [ -e $LOG_NAME.bag.active ]; do
  sleep 0.5;
done
# Add rosbag to tar file and compress
tar -rf ~/$LOG_NAME.tar $LOG_NAME.bag
rm $LOG_NAME.bag
gzip $LOG_NAME.tar

# Output
echo "Log package complete. Send the following file to Clearpath Support:"
echo "$LOG_NAME.tar.gz"
