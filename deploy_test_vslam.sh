# This script will deploy the robot and navigation workspaces needed to test the VSLAM system on blackwidow.
# This should be run on the robot.

# Delete the existing workspaces:
echo "Deleting Old Workspaces"
rm -fr ~/test_robot_ws ~/test_nav_ws

# Re-create the workspace  directories:
echo "Creating New Workspaces"
mkdir -p ~/test_robot_ws/src ~/test_nav_ws/src

# Copy the appropriate rosinstall files to the directories drectly from the git repos
echo "Copying blackwidow_robot rosinstall file to workspace"
git archive --remote=ssh://git@bitbucket.org/clearpathrobotics/blackwidow_robot master blackwidow_bringup/rosinstall/blackwidow_robot.rosinstall > bw.tar
tar -xvf bw.tar
cp blackwidow_bringup/rosinstall/blackwidow_robot.rosinstall ~/test_robot_ws/src/.rosinstall
rm bw.tar
rm -fr blackwidow_bringup

echo "Copying chec_nav rosinstall to workspace"
git archive --remote=ssh://git@bitbucket.org/clearpathrobotics/chec_navigation master rosinstall/chec_navigation.rosinstall > cn.tar
tar -xvf cn.tar
cp rosinstall/chec_navigation.rosinstall ~/test_nav_ws/src/.rosinstall
rm cn.tar
rm -fr rosinstall

# Initialize the workspace, populate the packages, and install dependencies with rosdep:
echo "Exporting ROSDISTRO_INDEX_URL and Performing rosdep update"
export ROSDISTRO_INDEX_URL=http://gitlab/sweng-infra/rosdistro_internal/raw/master/index.yaml
rosdep update

echo "Robot WS Initializing, Populating, and installing dependencies"
cd ~/test_robot_ws/src
catkin_init_workspace
wstool update -j8
rosdep install --from-path . -yir

echo "Nav  WS Initializing, Populating, and installing dependencies"
cd ~/test_nav_ws/src
catkin_init_workspace
wstool update -j8
rosdep install --from-path . -yir

# Build the workspaces:
echo "Build the Robot WS"
cd ~/test_robot_ws
catkin config --install
catkin config --cmake-args -DCMAKE_BUILD_TYPE=Release
catkin build

echo "Build the Navigation Workspace"
cd ~/test_nav_ws
catkin config --install
catkin config --cmake-args -DCMAKE_BUILD_TYPE=Release
catkin build
