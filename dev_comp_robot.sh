#!/bin/bash
echo   
echo ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
echo comparing $1 to $2
echo ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
echo
echo
echo ----------------------------------------------------------

echo
echo ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
echo Grabbing package lists
echo ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
echo
ssh -l administrator $1 "apt --installed list | tail -n+2" > ./$1.lst
ssh -l administrator $2 "apt --installed list | tail -n+2" > ./$2.lst
./compare_ubuntu_apt.pl $1.lst $2.lst

