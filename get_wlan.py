#!/usr/bin/env python
from pysnmp.entity import engine, config
from pysnmp.carrier.asynsock.dgram import udp
from pysnmp.entity.rfc3413 import cmdgen
from pysnmp.proto import rfc1902

from collections import namedtuple

tableOID = rfc1902.ObjectName('1.3.6.1.4.1.4329.20.1.1.1.1.27.1.1.11.1')

snmpEngine = engine.SnmpEngine()

config.addV1System(snmpEngine, 'my-area', 'public')

config.addTargetParams(snmpEngine, 'my-creds', 'my-area', 'noAuthNoPriv', 0)

config.addSocketTransport(
    snmpEngine,
    udp.domainName,
    udp.UdpSocketTransport().openClientMode()
)

config.addTargetAddr(
    snmpEngine, 'my-router',
    udp.domainName, ('10.254.254.254', 161),
    'my-creds'
)

OIDMapping = namedtuple('OIDMapping', 'oid attr converter')

class Connection(object):
    OID_TABLE = [
        OIDMapping(tableOID + (3,), 'channel', lambda v : int(v)),
        OIDMapping(tableOID + (4,), 'signal_strength', lambda v : int(v)),  # dBm
        OIDMapping(tableOID + (5,), 'signal_quality', lambda v : int(v)),   # %
        OIDMapping(tableOID + (6,), 'essid', lambda v : str(v)),
        OIDMapping(tableOID + (8,), 'connected', lambda v : int(v) == 1),   # disconnected == 2
    ]
    
    def __init__(self, mac):
        self.bssid = mac
        self.channel = None
        self.signal_strength = None
        self.signal_quality = None
        self.essid = None
        self.connected = False
        
    def add_oid(self, oid, val):
        for entry in self.OID_TABLE:
            if entry.oid.isPrefixOf(oid):
                setattr(self, entry.attr, entry.converter(val))
                return

    def __str__(self):
        return str(self.__dict__)
        
connection_table = {}


def oid_table_index_to_mac(oid):
    index = oid.asTuple()[-6:]
    return ':'.join('%02x' % v for v in index)

def cbFun(sendRequestHandle,
          errorIndication, errorStatus, errorIndex,
          varBindTable, cbCtx):
    if errorIndication:
        print(errorIndication)
        return
    # SNMPv1 response may contain noSuchName error *and* SNMPv2c exception,
    # so we ignore noSuchName error here
    if errorStatus and errorStatus != 2:
        print('%s at %s' % (
            errorStatus.prettyPrint(),
            errorIndex and varBindTable[-1][int(errorIndex)-1] or '?'
            )
        )
        return  # stop on error
    for varBindRow in varBindTable:
        for oid, val in varBindRow:
            if tableOID.isPrefixOf(oid):
                mac = oid_table_index_to_mac(oid)
                if mac not in connection_table:
                    connection_table[mac] = Connection(mac)
                else:
                    connection_table[mac].add_oid(oid, val)
                print('%s = %s' % (oid.prettyPrint(), val.prettyPrint()))
            else:
                for c in connection_table.values():
                    print(str(c))
                return False  # we have reached the end of the table
                
    return True # signal dispatcher to continue


cmdgen.NextCommandGenerator().sendReq(
    snmpEngine,
    'my-router',
    ( (tableOID, None),
    ),
    cbFun
)

snmpEngine.transportDispatcher.runDispatcher()
