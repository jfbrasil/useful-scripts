#!/bin/bash
# Script for reading Git hash of the current firmware from LVC dictionary.
# Requires libcanopen (https://github.com/rscada/libcanopen).

HASH=""

for i in 1 2 3 4 5; do
  HASH=${HASH}$(rs-canopen-sdo-upload can0 0a 3000 ${i})
done

# get version subindex from 0x1018 (Identity object)
VERSION_HEX="0x$(rs-canopen-sdo-upload can0 0a 1018 3)"
VERSION_NUM=`printf '%d' $VERSION_HEX`

MAJOR=`echo "$VERSION_NUM / (2^16)" | bc`
MINOR=`echo "$VERSION_NUM / (2^8) % (2^8)" | bc`
PATCH=`echo "$VERSION_NUM % (2^8)" | bc`

echo "Git hash: $HASH"
echo "Version:  $MAJOR.$MINOR.$PATCH"
