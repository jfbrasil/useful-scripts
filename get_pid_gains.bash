#!/bin/bash

function get_gain {
  NODE_ID=$1
  SUBINDEX=$2
  TYPE=$3
  SCALE=$4

  HEX="0x$(rs-canopen-sdo-upload can0 $NODE_ID 4651 $SUBINDEX)"
  VALUE=`printf '%d' ${HEX}`
  VALUE=`echo "$VALUE * $SCALE" | bc`

  echo -e "\t${TYPE} - hex: ${HEX}, scaled: ${VALUE}"
}

function get_gains {
  NODE_ID=$1

  # see if I can reach CAN device
  if rs-canopen-sdo-upload can0 $NODE_ID 5300 01 | grep ERROR > /dev/null; then
    echo -e "Could not reach CAN device with node id: $NODE_ID\n"
  else
    # log in
    rs-canopen-sdo-download can0 $NODE_ID 5000 02 4bdf

    get_gain $NODE_ID 01 P 0.000244141
    get_gain $NODE_ID 02 I 0.00390625
    get_gain $NODE_ID 06 D 0.00390625
    get_gain $NODE_ID 04 RollbackI 0.00390625
  fi
}

echo "Left Sevcon:"
get_gains 02

echo "Right Sevcon:"
get_gains 03
