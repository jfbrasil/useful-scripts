#!/usr/bin/env python

from pysnmp.entity import engine, config
from pysnmp.carrier.asynsock.dgram import udp
from pysnmp.entity.rfc3413 import cmdgen
from pysnmp.proto import rfc1902

from collections import namedtuple


ap_table_oid = rfc1902.ObjectName('1.3.6.1.4.1.4329.20.1.1.1.1.27.1.1.11.1')
        

OIDMapping = namedtuple('OIDMapping', 'oid attr converter')


class AccessPoint(object):
    OID_TABLE = [
        OIDMapping(ap_table_oid + (3,), 'channel', lambda v : int(v)),
        OIDMapping(ap_table_oid + (4,), 'signal_strength', lambda v : int(v)),  # dBm
        OIDMapping(ap_table_oid + (5,), 'signal_quality', lambda v : int(v)),   # %
        OIDMapping(ap_table_oid + (6,), 'essid', lambda v : str(v)),
        OIDMapping(ap_table_oid + (8,), 'connected', lambda v : int(v) == 1),   # disconnected == 2
    ]
    
    def __init__(self, mac):
        self.bssid = mac
        self.channel = None
        self.signal_strength = None
        self.signal_quality = None
        self.essid = None
        self.connected = False
        
    def add_oid(self, oid, val):
        for entry in self.OID_TABLE:
            if entry.oid.isPrefixOf(oid):
                setattr(self, entry.attr, entry.converter(val))
                return

    def __str__(self):
        return str(self.__dict__)


def oid_table_index_to_mac(oid):
    index = oid.asTuple()[-6:]
    return ':'.join('%02x' % v for v in index)


class SiemensAPTable(object):
    def __init__(self, bridge_ip, bridge_port=161):
        self.snmpEngine = engine.SnmpEngine()
        config.addV1System(self.snmpEngine, 'my-area', 'public')
        config.addTargetParams(self.snmpEngine, 'my-creds', 'my-area', 'noAuthNoPriv', 0)
        config.addSocketTransport(
            self.snmpEngine,
            udp.domainName,
            udp.UdpSocketTransport().openClientMode()
        )
        config.addTargetAddr(
            self.snmpEngine, 'my-router',
            udp.domainName, (bridge_ip, bridge_port),
            'my-creds'
        )
        self.aps = {}
        
    def update(self):
        cmdgen.NextCommandGenerator().sendReq(
            self.snmpEngine,
            'my-router',
            ((ap_table_oid, None),),
            self.snmp_callback
        )

        self.snmpEngine.transportDispatcher.runDispatcher()

    def snmp_callback(self, sendRequestHandle,
              errorIndication, errorStatus, errorIndex,
              var_bind_table, cbCtx):
        if errorIndication:
            print(errorIndication)
            return False
        # SNMPv1 response may contain noSuchName error *and* SNMPv2c exception,
        # so we ignore noSuchName error here
        if errorStatus and errorStatus != 2:
            print('%s at %s' % (
                errorStatus.prettyPrint(),
                errorIndex and var_bind_table[-1][int(errorIndex)-1] or '?'
                )
            )
            return False  # stop on error

        for var_bind_row in var_bind_table:
            for oid, val in var_bind_row:
                if ap_table_oid.isPrefixOf(oid):
                    mac = oid_table_index_to_mac(oid)
                    if mac not in self.aps:
                        self.aps[mac] = AccessPoint(mac)
                    else:
                        self.aps[mac].add_oid(oid, val)
                    # print('%s = %s' % (oid.prettyPrint(), val.prettyPrint()))
                else:
                    return False  # we have reached the end of the table, stop walk
            
        return True  # continue snmp walk
        

if __name__ == '__main__':
    ap_table = SiemensAPTable('10.254.254.254')
    ap_table.update()
    for ap in ap_table.aps.values():
        print(ap)

