# Will export the ROS_IP and ROS_MASTER_URI withone command automagically. 
# Call with $. master_setup.sh interface hostname
# interface is the ubuntu interface to the network (eth0 or wlan3)
# hostname is the hostname of the pc running the roscore you want to connect to

#!/bin/bash
export ROS_IP=`ifconfig $1 | grep 'inet addr:' | cut -d: -f2 | awk '{print $1}'`
export ROS_MASTER_URI=http://$2:11311
